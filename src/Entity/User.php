<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Episode", mappedBy="user")
     */
    protected $episodes;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->episodes = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->username;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get episodes
     *
     * @return Collection
     */
    public function getEpisodes(): Collection
    {
        return $this->episodes;
    }

    /**
     * Set episodes
     *
     * @param Collection $episodes
     *
     * @return User
     */
    public function setEpisodes(Collection $episodes)
    {
        $this->episodes = $episodes;

        return $this;
    }
}
